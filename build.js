let FS = require('fs'),
    PATH = require('path'),

    inlineCss = require('inline-css'),
    Minimize = require('minimize'),
    minimize = new Minimize(),
    pug = require('pug'),
    vow = require('vow');

if(!FS.existsSync('./dist')) FS.mkdirSync('./dist');

let promises = FS.readdirSync('./mails')
    .filter(function(file) {
        return PATH.extname(file) === '.pug' && ['_layout.pug', '_mixins.pug'].indexOf(file) === -1;
    })
    .map(function(file) {
        let defer = vow.defer();
        let templatePath = PATH.resolve(__dirname, 'mails', file);
        let html = pug.renderFile(templatePath);
        let fontLinks = html.match(/<link[^>]+?fonts\.googleapis\.com[^>]+?>/g) || [];
        let templateMarks = html.match(/\s*<%[-=]? [^>]+? %>\s*/g) || [];

        fontLinks.forEach(function(value, index) {
            html = html.replace(value, '<fontlink' + index + '></fontlink' + index + '>');
        });

        templateMarks.forEach(function(value, index) {
            html = html.replace(value, '<templatemark' + index + '></templatemark' + index + '>');
        });

        inlineCss(
            html,
            { url : 'file://' + templatePath }
        ).then(function(styledHtml) {
            // remove "class" attributes
            styledHtml = styledHtml.replace(/class=['"][^'"]*?['"]|class=\S*/ig, '');

            minimize.parse(styledHtml, function(error, minimizedHtml) {
                if(error) return defer.reject(error);

                fontLinks.forEach(function(value, index) {
                    minimizedHtml = minimizedHtml.replace(
                        '<fontlink' + index + '></fontlink' + index + '>', value);
                });

                templateMarks.forEach(function(value, index) {
                    minimizedHtml = minimizedHtml.replace(
                        '<templatemark' + index + '></templatemark' + index + '>', value);
                });

                // inline images into base64
                [
                    [/\.\.\/i\/[^/]+\.svg/g, 'data:image/svg+xml;base64,'],
                    [/\.\.\/i\/[^/]+\.png/g, 'data:image/png;base64,']
                ].forEach(value => {
                    let images = minimizedHtml.match(value[0]) || [];

                    // There is mailers, such as gmail, that not support Base64 images
                    // images.forEach(v => {
                    //     let base64 = FS.readFileSync(PATH.resolve('./mails', v)).toString('base64');
                    //     minimizedHtml = minimizedHtml.replace(v, value[1] + base64);
                    // });

                    images.forEach(v => {
                        minimizedHtml = minimizedHtml.replace(v,
                            'https://alios.gitlab.io/eloboss-mails/i/' + v.split('/').pop());
                    });
                });

                FS.writeFileSync(PATH.resolve(__dirname, 'dist', file.replace('.pug', '.html')), minimizedHtml);

                defer.resolve();
            });
        }, err => {
            defer.reject(err);
        });

        return defer.promise();
    });

vow.all(promises).then(function() {
    console.log('Build successful');
}).fail(function(error) {
    console.error(error);
});
