let FS = require('fs'),
    PATH = require('path'),
    renderMail = require('./index');

if(!FS.existsSync('distExamples')) FS.mkdirSync('distExamples');

let examplesArr = FS.readdirSync('./dataExamples').filter(function(file) {
    return PATH.extname(file) === '.json';
});

(function renderExample() {
    if(!examplesArr.length) {
        console.log('Build examples complete');
        return;
    }

    let file = examplesArr.pop();
    let emailConfig = JSON.parse(FS.readFileSync('./dataExamples/' + file).toString());

    console.log('Build example from: ' + file + ', template: ' + emailConfig.type);

    renderMail(emailConfig, function(error, html) {
        if(error) {
            console.log(error);
        } else {
            FS.writeFileSync('./distExamples/' + file.replace('.json', '.html'), html);
        }

        setTimeout(renderExample);
    });
}());
