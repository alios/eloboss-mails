let PATH = require('path'),
    ejs = require('ejs');

module.exports = function(data, cb) {
    let template = (data && data.type || 'common') + '.html';
    let templatePath = PATH.resolve(__dirname, 'dist', template);

    ejs.renderFile(templatePath, { data : data }, function(err, html) {
        if(err) return cb(err);

        cb(null, html);
    });
};
