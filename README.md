# Набор писем для проекта eloboss

- Письмо с приглашением на платформу [Шаблон](//alios.gitlab.io/eloboss-mails/dist/signup.html) | [Пример](//alios.gitlab.io/eloboss-mails/distExamples/signup.html)
- Отправка кода на верификацию почты при реге [Шаблон](//alios.gitlab.io/eloboss-mails/dist/confirmEmail.html) | [Пример](//alios.gitlab.io/eloboss-mails/distExamples/confirmEmail.html)
- Письмо о том что учетная запись юзера была создана (при ручной реге юзера) [Шаблон](//alios.gitlab.io/eloboss-mails/dist/completeRegister.html) | [Пример](//alios.gitlab.io/eloboss-mails/distExamples/completeRegister.html)
- Отправка кода на восстановление аккаунта [Шаблон](//alios.gitlab.io/eloboss-mails/dist/recovery.html) | [Пример](//alios.gitlab.io/eloboss-mails/distExamples/recovery.html)
- Письмо с инфой о заказе (если юзер не зареган) [Шаблон](//alios.gitlab.io/eloboss-mails/dist/purchaseCreated.html) | [Пример](//alios.gitlab.io/eloboss-mails/distExamples/purchaseCreated.html)
- Письмо с инфой о заказе (если юзер зареган уже) [Шаблон](//alios.gitlab.io/eloboss-mails/dist/purchaseCreated.html) | [Пример](//alios.gitlab.io/eloboss-mails/distExamples/purchaseCreatedRegistered.html)
- Письмо об том что оплачен заказ [Шаблон](//alios.gitlab.io/eloboss-mails/dist/purchasePaid.html) | [Пример](//alios.gitlab.io/eloboss-mails/distExamples/purchasePaid.html)
- Письмо о том что сменен purchase status [Шаблон](//alios.gitlab.io/eloboss-mails/dist/purchaseStatusChanged.html) | [Пример](//alios.gitlab.io/eloboss-mails/distExamples/purchaseStatusChanged.html)
- Письмо о том что сменен actual rank [Шаблон](//alios.gitlab.io/eloboss-mails/dist/rankChanged.html) | [Пример](//alios.gitlab.io/eloboss-mails/distExamples/rankChanged.html)
- Письмо о том что нужно оплатить заказ [Шаблон](//alios.gitlab.io/eloboss-mails/dist/purchasePay.html) | [Пример](//alios.gitlab.io/eloboss-mails/distExamples/purchasePay.html)
- Письмо повторное о том что нужно оплатить [Шаблон](//alios.gitlab.io/eloboss-mails/dist/purchasePayRepeat.html) | [Пример](//alios.gitlab.io/eloboss-mails/distExamples/purchasePayRepeat.html)
- Письмо о том что заказ завершен [Шаблон](//alios.gitlab.io/eloboss-mails/dist/purchaseComplete.html) | [Пример](//alios.gitlab.io/eloboss-mails/distExamples/purchaseComplete.html)
- Письмо о том что бустер добавлен в заказ [Шаблон](//alios.gitlab.io/eloboss-mails/dist/boosterAdded.html) | [Пример](//alios.gitlab.io/eloboss-mails/distExamples/boosterAdded.html)
- Письмо о том что бустер изменен в заказе [Шаблон](//alios.gitlab.io/eloboss-mails/dist/boosterChanged.html) | [Пример](//alios.gitlab.io/eloboss-mails/distExamples/boosterChanged.html)
- Письмо о том что первое сообщение в чате от бустера [Шаблон](//alios.gitlab.io/eloboss-mails/dist/boosterFirstMessage.html) | [Пример](//alios.gitlab.io/eloboss-mails/distExamples/boosterFirstMessage.html)
- Письмо о новом сообщении в чате от бустера [Шаблон](//alios.gitlab.io/eloboss-mails/dist/boosterNewMessage.html) | [Пример](//alios.gitlab.io/eloboss-mails/distExamples/boosterNewMessage.html)
- Письмо с данными от аккаунта (товар gdsItem) [Шаблон](//alios.gitlab.io/eloboss-mails/dist/accountData.html) | [Пример](//alios.gitlab.io/eloboss-mails/distExamples/accountData.html)

# Решаемые проблемы

* Хранение исходных шаблонов - письма должны быть легко редактируемыми.
* Inline css - стили из удобного для редактирования css файла должны быть автоматически заинлайнены в html.
* Base64 image - все используемые изображения(пока их мало) автоматически инлайнить в html(опция отключена, не все 
почтовики поддерживают base64, например gmail).
* Минификация html - все лишние пробелы, переводы строк, кавычки, ... и прочая необязательная разметка должна 
автоматически удаляться(итоговый html представляет из себя одну строку).
* Автоматическая сборка и хостинг примеров на пуш в репозиторий.

# Файловая система

````dataExamples```` - директория с примерами данных для построения полностью готовых писем

````dist```` - директория с рабочими шаблонами писем

````distExamples```` - директория с примерами готовых писем

````i```` - директория с используемыми изображениями(**важно**: хранить плоским списком, и в данный момент только svg)

````mails```` - директория с исходниками шаблонов

````build.js```` - скрипт для компиляции рабочих(дирректория dist) шаблонов

````buildExamples.js```` - скрипт для создания примеров писем с данными(дирректория distExamples)

# Установка

````npm install https://gitlab.com/alios/eloboss-mails.git````

# Как пользоваться

````js
let elobossMails = require('eloboss-mails');

elobossMails({
    "type" : "confirmEmail",
    "button" : {
        "text" : "404040",
        "url" : "confirm page url"
    }
}, function(err, html) {
    if(err) console.log(err);
    else console.log(html);
});
````
